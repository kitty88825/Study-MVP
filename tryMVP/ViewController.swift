//
//  ViewController.swift
//  try MVP
//
//  Created by NTUBIMD on 2018/3/19.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var message: UITextField!    
    @IBOutlet weak var label: UILabel!
    
    var rr = room()
    // －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
    @IBAction func enter(_ sender: UIButton) {
        label.text = rr.message(name: name.text!, content: message.text!)
        
    }
    
}

