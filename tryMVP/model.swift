//
//  model.swift
//  try MVP
//
//  Created by NTUBIMD on 2018/3/19.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import Foundation

protocol chat {
    func message(name:String, content:String) -> String
}

class room: chat {
    func message(name:String, content:String) -> String{
        return("\(name) : \(content)")
    }
}
